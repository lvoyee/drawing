# ⚡ Drawing 手绘风格图仓库 


#### 🚩 介绍
这里存放的都是手绘风格类型的各种图.

##### 1.cap原则 【韦恩图】
![cap原则](https://images.gitee.com/uploads/images/2021/1027/163826_3b3b153d_753205.png "cap原则.png")

##### 2.git代码分支管理流程图
![git代码分支管理流程图](https://images.gitee.com/uploads/images/2021/1102/110723_d4097c06_753205.png "git-flow.png")


##### 3.java设计模式
![java设计模式](https://images.gitee.com/uploads/images/2021/1111/101125_f5c47c48_753205.png "java_design_pattern.drawio.png")

##### 4.uml类之间的关系
![uml类之间的关系](https://images.gitee.com/uploads/images/2021/1116/145359_4902ebb5_753205.png "leitu_zj_20211116144939.png")

##### 5.1设计模式（单例模式）
![单例模式](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E5%8D%95%E4%BE%8B%E6%A8%A1%E5%BC%8F.png)

##### 5.2设计模式（原型模式）
![原型模式](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E5%8E%9F%E5%9E%8B%E6%A8%A1%E5%BC%8FPrototype.png)

##### 5.3设计模式（建造者模式）
![建造者模式](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E5%BB%BA%E9%80%A0%E8%80%85%E6%A8%A1%E5%BC%8F.png)

##### 5.4设计模式（工厂方法模式）
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E5%B7%A5%E5%8E%82%E6%96%B9%E6%B3%95%E6%A8%A1%E5%BC%8F.png)

##### 5.5设计模式 (抽象工厂模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E6%8A%BD%E8%B1%A1%E5%B7%A5%E5%8E%82%E6%A8%A1%E5%BC%8F.drawio.png)

##### 5.6设计模式 (桥接模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E6%A1%A5%E6%8E%A5%E6%A8%A1%E5%BC%8F.png)

##### 5.7设计模式 (组合模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E7%BB%84%E5%90%88%E6%A8%A1%E5%BC%8F.png)

##### 5.8设计模式 (装饰模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E8%A3%85%E9%A5%B0%E6%A8%A1%E5%BC%8F.png)

##### 5.9设计模式 (外观模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E5%A4%96%E8%A7%82%E6%A8%A1%E5%BC%8F.png)

##### 5.10设计模式 (享元模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E4%BA%AB%E5%85%83%E6%A8%A1%E5%BC%8F.png)

##### 5.11设计模式 (代理模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E4%BB%A3%E7%90%86%E6%A8%A1%E5%BC%8F.png)

##### 5.11设计模式 (适配器模式)
![输入图片说明](Drawing/%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8Fuml%E7%B1%BB%E5%9B%BE/%E9%80%82%E9%85%8D%E5%99%A8%E6%A8%A1%E5%BC%8F.png)


##### 6.markword对象头之64位
![输入图片说明](Drawing/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20230207103713.png)

##### 7.JVM相关1
![输入图片说明](%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20230223151020.png)

##### 8.JVM相关2
![输入图片说明](d9838cd6-1376-4d2b-98ea-ab6ac33da9bb.png)

##### 9.JVM相关3
![输入图片说明](JVM%E6%96%B9%E6%B3%95%E5%8C%BA.png)

##### 10.JVM相关4
![输入图片说明](JVM7.png)

#### 🚀 特别说明

所有图片存放在目录里面，如果侥幸被你看上，直接拿走即可!!!


----------------------------------------------------

作者:伊成


